"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This scripts is used to cluster the SIFT descriptors and therefore find those centers 
			 that are going to serve as key points to find the features of the SVM machine learning 
			 algorithm.
"""

import utilities as utl
import machine_learning as ml
import numpy as np
import sys


# Program starting point.
if __name__ == '__main__':
	if len(sys.argv) < 2: 
		print "Invalid number of arguments!"
		print "Please at least provide number of clusters for the image features."
	else: 
		# Reading number of clusters from the console.
		numberOfClusters = int(sys.argv[1])

		# Reading training images features
		print("Reading training images features")
		features = utl.readFeaturesFilesByClass(utl.TRAIN_IMAGES_FEATURES_FOLDER_PATH)

		totalFeaturesByClass = 5000
		# Merging the features before clustering
		featuresMerged = []
		for c, featuresSamples in features.items():
			samples = []
			for sample in featuresSamples:
				samples.extend(sample)
			np.random.shuffle(samples)
			featuresMerged.extend(samples[:totalFeaturesByClass])

		# Converting list into a matrix so it can be used in the clustering algorithm
		featuresMerged = np.array(featuresMerged)
		total, values = featuresMerged.shape

		# Clusters all the SIFT features to find the key points centers that are going to be used
		# in the SVM model
		
		print("Clustering...")
		assignment, centers = utl.measureRunningTime(lambda : 
			ml.kMeansClusteringAlgorithm(\
				featuresMerged, 
				numberOfClusters,
				lambda x: x,
				total,
				ml.pickClusterCentersRandomly,
				10	
			)
		)


		print "\n\n\nCluster Centers"
		print centers

		print("\n\nSaving clusters centers")
		# Saving the clusters centers.
		utl.dumpAtFile(centers, utl.CLUSTER_CENTERS_FILE_PATH)
		print("Done!")