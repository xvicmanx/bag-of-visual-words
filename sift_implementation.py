"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script contains the implementation of the SIFT algorithm.
"""
import cv2
import numpy as np
import filters as fi
from scipy import ndimage as ndi
from collections import Counter


def findIntensityGradients(image):
	"""
	Finds the intensity and direction gradients of  an image
	parameters: image ->  image to find intensity and direction gradients
	returns: the intensity and direction gradients
	"""
	# Finding derivatives on both directions
	gx, gy = firstDerivatives(image)
	theta = np.arctan2(gy, gx) * (180 / np.pi)
	# changing angle values to be between 0-360 degrees
	changeRangeOfAngles = np.vectorize(lambda x: 360.0 + x if x < 0 else x)
	theta = changeRangeOfAngles(theta)
	# Calculating magnitude
	g = (gx**2 + gy**2)**0.5
	maximum = np.amax(g)
	if maximum > 0:
		g = (g/maximum)*255
	return g, theta


def findHistogram(area, numberOfBins):
	"""
	Find the histogram of gradient orientations in an area.
	Parameters:
				area -> area to find the histogram of orientations
	Returns:	a vector representing the histogram.
	"""
	counts = Counter(area.ravel().tolist())
	result = np.zeros(numberOfBins)
	angle = int(360/numberOfBins)
	for index in counts: 
		result[index/angle] = counts[index] 
	return result


def findWeightedHistogram(\
	magnitude,
	angles,
	numberOfBins
):
	"""
	Find the histogram of gradient orientations in an area.
	Parameters:
				angles
				magnitude
				numberOfBins
	Returns:	a vector representing the histogram.
	"""
	result = np.zeros(numberOfBins)
	for i in range(numberOfBins):
		result[i] = np.sum(\
			magnitude[np.where((angles/numberOfBins)==i)]
		)
	return result



def generateScales(image, numberOfScales = 5):
	"""
	Create the number of scales of an image by repeating the image
	the desired number of times.
	Parameters:
				image -> image where the scales are going to 
						 be created from.
				numberOfScales -> number of scales to be created.
	Returns: the generated scale.
	"""
	rows, cols = image.shape
	scales = [image for i  in xrange(numberOfScales)]
	return scales

def generateGaussians(scales, sigma, kernelSize = 13):
	"""
	Given list of matrices representing the scales,
	applies the increasing gaussian by scale level.
	Parameters:
				scales -> scales to apply the gaussians.
				sigma  -> initial sigma.
				kernelSize -> size of gaussian kernel.
	Returns: the result after applying the gaussian by level.
	"""
	k = np.sqrt(2)
	numberOfScales = len(scales)
	gaussians = []

	for i in xrange(numberOfScales):
		blurred = fi.gaussianBlur(\
			scales[i],
		 	kernelSize,
		 	sigma
		 )
		gaussians.append(blurred)
		sigma = k*sigma

	return gaussians


def getScalesLOG(gaussians):
	"""
	Using the gaussians from scales, finds the LOG along 
	the scale.
	Parameters: 
				gaussians ->  gaussians from the scale
	Returns: the laplacians of gaussians
	"""
	laplacians = []
	for i in xrange(1, len(gaussians)):
		laplacian = cv2.subtract(gaussians[i], gaussians[i-1])
		laplacians.append(laplacian)
	return laplacians


def  __canBeAppliedNeighborhoodInArea(row, col, rows, cols, radius):
	"""
	Checks if neighboorhood can be applied around the point specied by the values 
	row, col. 
	Parameters:
				(row, col) -> values that specify the point where is desired
							  to know if a neighboorhood can be applied.
				(rows, cols) -> dimension values of the image.
	"""
	# Checking if the area does not get out of bounds.
	if row - radius < 0  or row - radius < 0: return False
	if row + radius >= rows  or col + radius >= cols: return False
	return True

def detectKeyPoints(image):
	"""
	Detect SIFT points in an image.
	Parameters:
				image -> image to detect the key points from.
	Returns: a list of detected points by octave.
	"""
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	numberOfOctaves = 4
	k = np.sqrt(2)
	sigma = 1.6
	neighborhoodSize = 1

	possibleKeyPoints = []
	footprint = np.ones((3, 3))
	footprint[1, 1] = 0 
	octaveGaussians = []

	for octave in xrange(numberOfOctaves):
		scales = generateScales(gray)
		gaussians = generateGaussians(scales, sigma)
		octaveGaussians.append(gaussians)

		scalesLOG = getScalesLOG(gaussians)
		for i in xrange(0, len(scalesLOG)):
			currentMaximum = ndi.maximum_filter(\
				scalesLOG[i],
				footprint=footprint
			)
			currentMinimum = ndi.minimum_filter(\
				scalesLOG[i],
				footprint=footprint
			)
			
			dxx, dyy, dxy = secondDerivatives(gaussians[i])
			r =  np.divide(dxx.astype(float), dyy+1e-10)
			
			if i!=0 and i!=len(scalesLOG)-1:
				maximumPrev = ndi.maximum_filter(\
					scalesLOG[i-1],
					size=neighborhoodSize
				)
				minimumPrev = ndi.minimum_filter(\
					scalesLOG[i-1],
					size=neighborhoodSize
				)
			
				maximumNext = ndi.maximum_filter(\
					scalesLOG[i+1],
					size=neighborhoodSize
				)
				minimumNext = ndi.minimum_filter(\
					scalesLOG[i+1],
					size=neighborhoodSize
				)

				
				isExtrema = np.logical_or(
						np.logical_and(
							current > maximumPrev,
							current > maximumNext,
							current > currentMaximum
						),
						np.logical_and(
							current < minimumPrev,
							current < minimumNext,
							current < currentMinimum
						)
				)

				current = scalesLOG[i]
				opkp =  np.where(\
					np.logical_and(
						isExtrema,
						r <=10
					)
				)
				
				possibleKeyPoints.append((octave, i, opkp))
			elif i==0:
				maximumNext = ndi.maximum_filter(\
					scalesLOG[i+1],
					size=neighborhoodSize
				)
				minimumNext = ndi.minimum_filter(\
					scalesLOG[i+1],
					size=neighborhoodSize
				)

				current = scalesLOG[i]
				isExtrema = np.logical_or(
					np.logical_and(
						current > maximumNext,
						current > currentMaximum
					),
					np.logical_and(
						current < minimumNext,
						current < currentMinimum
					)
				)

				opkp =  np.where(\
					np.logical_and(
						isExtrema,
						r <=10
					)
				)
				
				possibleKeyPoints.append((octave, i, opkp))
			elif i==len(scalesLOG)-1:
				maximumPrev = ndi.maximum_filter(\
					scalesLOG[i-1],
					size=neighborhoodSize
				)
				minimumPrev = ndi.minimum_filter(\
					scalesLOG[i-1],
					size=neighborhoodSize
				)

				current = scalesLOG[i]
				isExtrema = np.logical_or(
					np.logical_and(
						current > maximumPrev,
						current > currentMaximum
					),
					np.logical_and(
						current < minimumPrev,
						current < currentMinimum
					)
				)

				opkp =  np.where(\
					np.logical_and(
						isExtrema,
						r <=10
					)
				)
				
				possibleKeyPoints.append((octave, i, opkp))

		sigma = (k**2)*sigma
		gray = cv2.pyrDown(gray)

	keyPoints = []
	for i in range(len(possibleKeyPoints)):
		
		octave, scaleIndex, opkp = possibleKeyPoints[i]

		if len(octaveGaussians) <= scaleIndex: continue

		# calculating gradient magnitude and angles in the image
		magnitude, angles = findIntensityGradients(\
			octaveGaussians[octave][scaleIndex]
		)
		# Converting all the angles to 8 values multiple of 10 degrees
		digitizeAllAngles = np.vectorize(lambda x: (int(x)/10)*10)
		angles = digitizeAllAngles(angles)
		

		for p in zip(opkp[0], opkp[1]):

			row, col = p[0], p[1]
			rows, cols = magnitude.shape
			radius = 2
			if __canBeAppliedNeighborhoodInArea(\
				row,
				col,
				rows,
				cols,
				radius
			):

				m = magnitude[row-radius:row+radius,col-radius:col+radius]
				a = angles[row-radius:row+radius,col-radius:col+radius]
				histogram = findWeightedHistogram(m, a, 36)
				angle =  np.argmax(histogram) * 10
				
				kp = cv2.KeyPoint(\
					col, row, 5, angle, 0.5,  octave
				) 
				keyPoints.append(kp)
	return keyPoints


def secondDerivatives(image):
	"""
	Computes the second order derivates of an image.
	Parameters:
				image -> image to compute the second order
						 derivatives from.
	Returns: the second order derivatives.
	"""
	dxx = cv2.Sobel(image,cv2.CV_64F, 2, 0, ksize=3) 
	dyy = cv2.Sobel(image,cv2.CV_64F, 0, 2, ksize=3)
	dxy = cv2.Sobel(image,cv2.CV_64F, 1, 1, ksize=3) 
	return dxx, dyy, dxy


def firstDerivatives(image):
	"""
	Computes the fist order derivates of an image.
	Parameters:
				image -> image to compute the first order
						 derivatives from.
	Returns: the first order derivatives.
	"""
	dx = cv2.Sobel(image,cv2.CV_64F, 1, 0, ksize=3) 
	dy = cv2.Sobel(image,cv2.CV_64F, 0, 1, ksize=3)
	return dx, dy


def __findFeatureVector(area):
	"""
	Finds the HOG feature vector descriptor in a given block area.
	Parameters:
				area -> area to find the HOG descriptor.
	"""
	featureVector = np.zeros(128)
	# The area is subdivided in 16 4x4 subregions
	# for each subregion the histogram of gradient orientations is calculated
	# and stored in the feature vector result.
	for position in range(16):
		verticalIndex = position/4
		horizontalIndex = position%4
		startRow = verticalIndex*4
		startCol = horizontalIndex*4
		# Taking subregion from the main area
		subRegion = area[startRow:startRow+4, startCol:startCol+4]
		# Finding histogram of orientations in that subregion..
		featureVector[position*8:position*8+8] = findHistogram(subRegion, 8)
	return featureVector


def computeDescriptors(image, keyPoints):
	"""
	Computes SIFT Descriptors in the keyPoints detected in an image.
	Parameters: 
				image -> image to compute descriptors from.
				keyPoints -> keyPoints detected in the image.
	Returns: the computed descriptors.
	"""
	pyramid = []
	current = image
	# Converting all the angles to 8 values multiple of 45 degrees
	digitizeAllAngles = np.vectorize(lambda x: (int(x)/45)*45)
	
	for i in xrange(4):
		magnitude, angles = findIntensityGradients(\
			current
		)
		
		current = cv2.pyrDown(current)
		pyramid.append(angles)

	descriptors = []
	validKeyPoints = []
	for keyPoint in keyPoints:
		col, row = keyPoint.pt
		octave = keyPoint.octave
		rows, cols = pyramid[octave].shape
		
		if __canBeAppliedNeighborhoodInArea(\
			row,
			col,
			rows,
			cols, 
			8
		):
			radius = 8
			window = pyramid[octave][row-radius:row+radius,col-radius:col+radius]
			
			if window.size == 0 : continue
			
			changeRangeOfAngles = np.vectorize(lambda x: 360.0 + x if x < 0 else x)
			window = window - keyPoint.angle
			theta = changeRangeOfAngles(window)
			theta = digitizeAllAngles(theta)
			feature = __findFeatureVector(theta)
			descriptors.append(feature)
			validKeyPoints.append(keyPoint)

	return validKeyPoints, descriptors
