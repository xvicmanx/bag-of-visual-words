"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: Script to store all the image features extraction functions.
"""

import cv2
import filters as fi
import math
import numpy as np
import cv2 
from scipy import ndimage as ndi
import machine_learning as ml
import sift_implementation as si


def getSIFTFeatures(image):
	"""
	Finds the SIFT features of a given image.
	Parameters: 
				image -> image to find the SIFT features.
	Returns: 	the SIFT features of the image.
	"""

	keyPoints =  si.detectKeyPoints(image)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	keyPoints, descriptors = si.computeDescriptors(\
		gray,
		keyPoints
	)
	return descriptors


def getClassifierFeatures(imageFeatures, centers):
	"""
	Generates the features for a given sample by finding 
	the histogram of the clusters centers.
	Parameters:
				imageFeatures -> image's features
				centers 	  -> clusters centers
	Returns:	the generated features.
	"""
	centersRow, centersCol = centers.shape
	numberOfBins = centersRow

	assignment = ml.findClusterAssingment(\
		np.array(imageFeatures),
		centers
	)
	
	histogram, binsInformation = np.histogram(\
		assignment.ravel(),
		numberOfBins,
		[0, numberOfBins]
	)

	return histogram