"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script is used to separate the images into train and test images.
"""

import utilities as utl
import os
import numpy as np
import shutil

def createFolderIfNotExist(folderPath):
	# Checking if dir exist and creating it if does not exist
	try: os.stat(folderPath)
	except: os.mkdir(folderPath) 

# Cleaning dir first
utl.cleanFolder(utl.TRAIN_IMAGES_FOLDER_PATH)
utl.cleanFolder(utl.TEST_IMAGES_FOLDER_PATH)


# Getting image files by class
allImagesFiles = utl.getImageFileNamesByClass(utl.ALL_IMAGES_FOLDER_PATH)


for className in allImagesFiles:
	print className
	trainPath = "{0}{1}".format(utl.TRAIN_IMAGES_FOLDER_PATH, className)
	testPath = "{0}{1}".format(utl.TEST_IMAGES_FOLDER_PATH, className)

	createFolderIfNotExist(trainPath)
	createFolderIfNotExist(testPath)

	np.random.shuffle(allImagesFiles[className])
	total = len(allImagesFiles[className])
	total = min(150, total)
	trainValues = int(0.8 * total)
	trainSet = allImagesFiles[className][:trainValues]
	testSet = allImagesFiles[className][trainValues:total]


	for fileName in trainSet:
		shutil.copyfile(fileName, "{0}/{1}".format(trainPath,  os.path.basename(fileName)))

	for fileName in testSet:
		shutil.copyfile(fileName, "{0}/{1}".format(testPath,  os.path.basename(fileName)))

	
	# print allImagesFiles[className]