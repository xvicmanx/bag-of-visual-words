"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This scripts contains some of the helpers functions used through the entire project.

"""

import pickle
import os
import features_extraction_utilities as fe
import cv2
import time
import numpy as np
import shutil

def dumpAtFile(data, filePath):
	"""
	Saves an object as pickle file.
	Parameters:
				data ->  object to be Saved
				filePath -> file location where to stored the pickle.
	"""
	outputFile = open(filePath, 'wb')
	pickle.dump(data, outputFile)
	outputFile.close()


def readPickeAtLocation(filePath):
	"""
	Reads pickle file from location.
	Parameters:
				filePath -> location of the pickle file
	Returns: the object stored in the pickle file.
	"""
	inputFile = open(filePath, 'rb')
	result = pickle.load(inputFile)
	inputFile.close()
	return result


def getImageFileNamesByClass(folderPath):
	"""
	Reads all the image filenames from all the folders that represent the classes.
	Parameters:
				folderPath -> root folder of the images by class.
	Returns: a dictionary where the keys are the classes and values are a list of
			 images files names.
	"""
	imageFiles = {}
	for (dirpath, dirnames, filenames) in os.walk(folderPath):
		for dirname in dirnames:
			for (childDirpath, childDirnames, childFilenames) in os.walk(dirpath + "/" + dirname):
		   	 	imageFiles[dirname] = [childDirpath + "/" + childFilename for  childFilename in childFilenames]
	return imageFiles

def extractFeaturesByClass(imageFilesNamesByClass):
	"""
	Given a dictionary  where the keys are the classes and values are a list of
	images files names, extracts the features by each image.
	Parameters:
				imageFilesNamesByClass -> dictionary that specifies the images files
										  by class.
	Returns : a dictionary where the keys are the classes and values are a list of
			  images SIFT features. 								
	"""
	imagesFeatures = {}
	for className, imageFiles in imageFilesNamesByClass.items():
		imagesFeatures[className] = []
		for imageFile in imageFiles:
			image = resizeToHeight(cv2.imread(imageFile), 200)
			features = fe.getSIFTFeatures(image)
			imagesFeatures[className].append(features)
	return imagesFeatures




def saveFeaturesFilesByClass(imagesFeatures, folder):
	"""
	Given a dictionary  where the keys are the classes and values are a list images'
	SIFT features. Stores this features in files by image in folders by class.

	Parameters:
				imagesFeatures -> dictionary that has the image features by class.
				folder         -> root folder to store the files.								
	"""
	for className, featuresData in imagesFeatures.items():
		count = 0	
		for featuresSample in featuresData:
			count+=1
			dirName = "{0}{1}/".format(folder, className)
			# Checking if dir exist and creating it if does not exist
			try: os.stat(dirName)
			except: os.mkdir(dirName)  

			dumpAtFile(\
				featuresSample,
				"{0}{1}.pkl".format(dirName, count)
			)


def readFeaturesFilesByClass(folderPath):
	"""
	Reads the images' stored features from the given root folder.

	Parameters:
				folderPath        -> path of root folder where the files are stored.		
	Returns:  a dictionary where the keys are the classes and values are a list of
			  images SIFT features by cass.					
	"""
	imagesFeatures = {}
	WalkFunction = os.walk
	for (dirpath, dirnames, filenames) in WalkFunction(folderPath):
		for dirname in dirnames:
			imagesFeatures[dirname] = []
			ListAppend = imagesFeatures[dirname].append
			for (childDirpath, childDirnames, childFilenames) in WalkFunction(dirpath + "/" + dirname):
				for childFilename in childFilenames:
					data = readPickeAtLocation(childDirpath + "/" + childFilename)
		   	 		ListAppend(data)

	return imagesFeatures


def cleanFolder(folderPath):
	for root, dirs, files in os.walk(folderPath):
		for dirName in dirs:
			shutil.rmtree("{0}{1}".format(root, dirName))


def measureRunningTime(operation):
	"""
	Measures the time that takes an operation
	"""
	start = time.time()
	result = operation()
	end = time.time()
	print  "Took {0} seconds.\n\n".format(end - start)
	return result


def addText(image, text, location=(3, 30), fontSize = 1, thickness=1,  color=None):
	"""
	Adds text to an image. (Based on example from last assignment)
	Parameters: 
				image -> desired image to add text on.
				text  -> text to be added on the image.
				color -> color of the text ((R, G, B) integer tuple)
	Returns: a copy of the image with text on it.
	"""
	color = (0, 0, 255) if color is None else color
	copy = np.copy(image)
	font = cv2.FONT_HERSHEY_SIMPLEX
	cv2.putText(copy, text, location, font, fontSize, color, thickness)
	return copy


def resizeToHeight(image, height):
	"""
	Scales an image to have the desired height.
	Parameters: image -> image to be scaled.
				height -> the desired height of the image.
	Returns: Returns the scaled image.
	"""
	ratio = float(image.shape[1])/image.shape[0]
	return cv2.resize(image, (int(ratio*height), height))

# Directory of the script file
currentDirectory = os.path.dirname(os.path.abspath(__file__))

# Constant of the folders path.
ALL_IMAGES_FOLDER_PATH = "{0}/all_images".format(currentDirectory)
TRAIN_IMAGES_FOLDER_PATH = "{0}/images/train/".format(currentDirectory)
TRAIN_IMAGES_FEATURES_FOLDER_PATH = "{0}/data/image_features/train/".format(currentDirectory)
TEST_IMAGES_FOLDER_PATH = "{0}/images/test/".format(currentDirectory)
TEST_IMAGES_FEATURES_FOLDER_PATH = "{0}/data/image_features/test/".format(currentDirectory)
CLUSTER_CENTERS_FILE_PATH = "{0}/data/cluster_centers.pkl".format(currentDirectory)
CLASSIFIER_FILE_PATH = "{0}/data/classifier.pkl".format(currentDirectory)