"""
Author: Karteek Bulusu  and Victor Trejo 
Description: This script contains the implementation of the unsupervised K-Means clustering algorithm
"""
import cv2
import numpy as np
from matplotlib import pyplot as pl



def findClusterAssingment(values, centers):
	"""
	Finds the cluster assignment of each vector value
	based on the euclidean distance.
	"""
	valuesRows, valuesColumns = values.shape
	centersRows, centersCols = centers.shape

	assignment = np.zeros((valuesRows, 1))

	ones = np.ones((valuesColumns, 1))

	# Saving reference for speeding up
	NPRepeat = np.repeat
	NPARGMIN = np.argmin
	NPSQRT= np.sqrt
	NPSQUARE = np.square
	NPSUM = np.sum
	for i in range(valuesRows):
		distances = NPRepeat(values[i].reshape(1, valuesColumns), centersRows, axis=0)
		# Assigning closest cluster
		assignment[i]  = NPARGMIN(\
			NPSQRT(
				NPSUM(
						NPSQUARE(distances - centers),
						 axis=1
				)
			)
		)
		
	return assignment



def pickClusterCentersRandomly(vector, numberOfClusters):
	"""
	Picker function to choose the initial cluster centers
	from the vector data randomly.
	Parameters:
				vector -> vector where the clusters centers 
						  are going to be extracted
				numberOfClusters -> number of cluster centers to choose.
	Returns: the chosen cluster centers.
	"""
	pull = np.vstack({tuple(element) for element in vector})
	np.random.shuffle(pull)
	return pull[:numberOfClusters,:]


def kMeansClusteringAlgorithm(\
	vector,
	numberOfClusters,
	extractorFunction,
	length,
	clusterCentersPickerFunction,
	numberIterations = 5,
	printCenters = False
):
	"""
	Applies K-Means Clustering algorithm to a given vector.
	Parameters:
				vector 			  -> vector to be clustered.
				numberOfClusters  -> number of cluster in wich the data will be divided.
				extractorFunction -> function that helps to extract the features of each vector value 
									 for comparison to the centers.
				length 			  -> number of values in the vector
				clusterCentersPickerFunction -> helper function to Initially select the clusters centers
				numberIterations -> max number of iterations that the algorithm will do if it does not converge
				printCenters     -> a flag to print or not the cluster centers when running the algorithm
	Returns: the cluster assignment to each vector value and the cluster centers
	"""
	# Initially picking the cluster centers
	clusterCenters = clusterCentersPickerFunction(\
		vector,
		numberOfClusters
	)
	
	if printCenters:
		print "Initial cluster Centers {}\n".format(clusterCenters)

	# Initially no point has cluster assigned
	clusterAssignment = np.ones(length) * -1

	# To keep track the previous values of the cluster centers
	previousClusterCenters = np.ones(clusterCenters.shape) * -1
	# The algorithm will iterate until the cluster centers do not change
	# or it runs out the number of iterations (iteration=number of iterations)
	iteration = 1
	while (clusterCenters!=previousClusterCenters).all() and\
		iteration <= numberIterations:
		# Saving the current values of the cluster centers
		# to following compare them to the new cluster centers,
		# an see if there is convergence
		previousClusterCenters = clusterCenters.copy()
		clusterAssignment = findClusterAssingment(\
			vector,
			clusterCenters
		).flatten()
	
		# Updating the cluster centers to be the average of the 
		# values that belong to each cluster
		for k in range(numberOfClusters):
			values = vector[clusterAssignment==k,:]
			if values.size > 0:
				clusterCenters[k] = np.mean(values, axis=0)

		print "Iteration {}\n".format(iteration)
		iteration+=1

		if printCenters:
			print  "Cluster Centers:\n\n {}\n".format(clusterCenters)
		


	return clusterAssignment, clusterCenters

