"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: Script to store all the image filters functions.
"""

import cv2

import math
import numpy as np
import cv2 
from scipy import ndimage as ndi


def __gaussian(value, std):
	"""
	Computes the gaussian function for a given value 
	the  given standar deviation
	params: 
			value -> value to calculate the gaussian
			std -> the standar deviation   
	Returns : the result of computing the gaussian function for the given
			  parameters.
	"""
	coeficient = (float(1)/(math.sqrt(2 * math.pi) * std))
	return coeficient * math.exp(-float(value**2)/(2*std**2))

def __derivativeOfGaussian(value, std):
	"""
	Computes the derivative of gaussian function for a given value 
	the  given standar deviation
	params: 
			value -> value to calculate the gaussian
			std -> the standar deviation   
	Returns : the result of computing the gaussian function for the given
			  parameters.
	"""
	coeficient = (-float(value)/(math.sqrt(2 * math.pi) * std**3))
	return coeficient * math.exp(-float(value**2)/(2*std**2))	


def __getGaussianKernel(size, std):
	"""
	Constructs a 1-D Gaussian kernel.
	Parameters:
				size -> size of the kernel
				std  -> standard deviation gaussian
	Returns: the gaussian kernel.
	"""
	gaussian = [\
		__gaussian(x-size/2, std) 
		for x in range(size)
	]
	return np.matrix([gaussian])

def __getGaussianDerivativeKernel(size, std):
	"""
	Constructs a 1-D derivative gaussian kernel.
	Parameters:
				size -> size of the kernel
				std  -> standard deviation gaussian
	Returns: the gaussian kernel.
	"""
	gaussian = [\
		__derivativeOfGaussian(x-size/2, std) 
		for x in range(size)
	]
	return np.matrix([gaussian])


def __applyFilter(image, kernel):
	"""
	Applies a filter specified in the kernel to an image
	Parameters:
				image  -> image to be filtered
				kernel -> kernel to be applied
	Returns: the filtered image.
	"""
	return cv2.filter2D(image, -1, kernel) 	


def gaussianBlur(image, size, std):
	"""
	Applyies gaussian blur filter to an image.
	Parameters: 
				image -> image to blur
				size  -> size of the gaussian
				std   -> standard deviation of the gaussian
	Returns: returns the image after blurrying.
	"""
	kernel = __getGaussianKernel(size, std)
	# Applying gaussian filter:
	# Using separability property of gaussian
	# It is the same as applying two gaussians:
	# in x and y.
	return __applyFilter(\
		__applyFilter(image, kernel), 
		kernel.T
	)



def applyGaussianXDerivative(image, size, std):
	"""
	Applyies gaussian derivative x to an image.
	Parameters: 
				image -> image to apply derivative
				size  -> size of the gaussian
				std   -> standard deviation of the gaussian
	Returns: returns the image after deriving.
	"""
	xGaussianDerivativeKernel = __getGaussianDerivativeKernel(size, std)
	yGaussianKernel = __getGaussianKernel(size, std).T
	# Applying gaussian filter:
	# Using separability property of gaussian
	# It is the same as applying two gaussians:
	# in x and y.
	return __applyFilter(\
		__applyFilter(image, xGaussianDerivativeKernel), 
		yGaussianKernel
	)


def applyGaussianYDerivative(image, size, std):
	"""
	Applyies gaussian derivative y to an image.
	Parameters: 
				image -> image to apply derivative
				size  -> size of the gaussian
				std   -> standard deviation of the gaussian
	Returns: returns the image after deriving.
	"""
	xGaussianKernel = __getGaussianKernel(size, std)
	yGaussianDerivativeKernel = __getGaussianDerivativeKernel(size, std).T
	# Applying gaussian filter:
	# Using separability property of gaussian
	# It is the same as applying two gaussians:
	# in x and y.
	return __applyFilter(\
		__applyFilter(image, xGaussianKernel), 
		yGaussianDerivativeKernel
	)



def applyXDerivative(image):
	
	return (float(1)/8) * __applyFilter(\
		__applyFilter(image, np.matrix([[1, 2, 1]]).T), 
		np.matrix([[-1, 0, 1]])
	)

def applyYDerivative(image):
	
	return (float(1)/8) * __applyFilter(\
		__applyFilter(image, np.matrix([[-1, 0, 1]]).T), 
		np.matrix([[1, 2, 1]])
	)
