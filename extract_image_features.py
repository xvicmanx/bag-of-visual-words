"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This scripts is used to extract SIFT features from the images so 
they can then be used to train and test the models.
"""
import utilities as utl


# Extracting features for the Training images
print("Extracting features for the Training images")
# Cleaning dir first
utl.cleanFolder(utl.TRAIN_IMAGES_FEATURES_FOLDER_PATH)

# Getting image files by class
trainImageFiles = utl.getImageFileNamesByClass(utl.TRAIN_IMAGES_FOLDER_PATH)
# For all the images by class the features will be extracted
trainImageFeatures = utl.extractFeaturesByClass(trainImageFiles)
print("Saving extracted features from Traininig images")
# These features then will be stored in files to be used in the future
utl.saveFeaturesFilesByClass(\
	trainImageFeatures,
	utl.TRAIN_IMAGES_FEATURES_FOLDER_PATH
)


# Extracting features for the Test images
print("Extracting features for the Test images")
# Cleaning dir first
utl.cleanFolder(utl.TEST_IMAGES_FEATURES_FOLDER_PATH)		

# Getting image files by class
testImageFiles = utl.getImageFileNamesByClass(utl.TEST_IMAGES_FOLDER_PATH)
# For all the images by class the features will be extracted
testImageFeatures = utl.extractFeaturesByClass(testImageFiles)
print("Saving extracted features from Test images")
# These features then will be stored in files to be used in the future
utl.saveFeaturesFilesByClass(\
	testImageFeatures,
	utl.TEST_IMAGES_FEATURES_FOLDER_PATH
)

print("Done!")