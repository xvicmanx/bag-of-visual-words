"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script is used to train the classifier.
"""

import utilities as utl
import machine_learning as ml
import features_extraction_utilities as fe
import numpy as np
from sklearn.svm import LinearSVC
import pprint

print("Reading cluster centers")
# Reading the clusters centers.
centers = utl.readPickeAtLocation(\
	utl.CLUSTER_CENTERS_FILE_PATH
)

print("Reading training images features")
# Reading training images features
featuresByClass = utl.readFeaturesFilesByClass(\
	utl.TRAIN_IMAGES_FEATURES_FOLDER_PATH
)

print("Getting training features")
trainingModelData = []
GetClassifierFeatures = fe.getClassifierFeatures
AppendToList = trainingModelData.append
for className in featuresByClass:
	for sampleFeatures in featuresByClass[className]:
		print "Training sample {}".format(len(trainingModelData))
		trainFeatures = GetClassifierFeatures(sampleFeatures, centers)
		AppendToList((list(trainFeatures), className))


# Separating training data into features values and classes
features = map(lambda x: x[0], trainingModelData)
classes = map(lambda x: x[1], trainingModelData)

print("Training classifier model")
# Creating classifier and training it
classifier = LinearSVC(C=0.005)
# from sklearn.naive_bayes import MultinomialNB
# classifier = MultinomialNB()
classifier.fit(features, classes)

# Storing classifier model
print("Storing classifier model")
utl.dumpAtFile(classifier, utl.CLASSIFIER_FILE_PATH)

