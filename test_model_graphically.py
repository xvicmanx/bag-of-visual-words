"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script is used to show the testing of the model graphically.
"""
import utilities as utl
import features_extraction_utilities as fe
import numpy as np
import cv2


def resizeToHeight(image, height):
	"""
	Scales an image to have the desired height.
	Parameters: image -> image to be scaled.
				height -> the desired height of the image.
	Returns: Returns the scaled image.
	"""
	ratio = float(image.shape[1])/image.shape[0]
	return cv2.resize(image, (int(ratio*height), height))


def preparePresentationImage(\
	image,
	originalClass,
	predictedClass
):
	"""
	Prepares the image before been presented.
	Parameters:
				image 		  -> image of Object
				originalClass -> the class of the Object
				predictedClass -> the class predicted by the algorithm.
	Returns: the prepared image.
	"""
	resized = cv2.resize(image, (512, 512))
	# resized = cv2.rectangle(resized,(0, 0),(512, 50),(0,0,0),-1)
	black = resized.copy()
	black[:,:, :] = 0
	labeledImage = utl.addText(\
		black,
		"Predicted: " + predictedClass,
		(30, 160),
		1,
		3,
		(0, 255, 255)
	)

	labeledImage = utl.addText(\
		labeledImage,
		"Original: " + originalClass,
		(30, 70),
		1,
		3,
		(255, 255, 255)
	)

	result = np.hstack((resized, labeledImage))
	text = "Correct!" if originalClass==predictedClass else "Incorrect!"
	result = cv2.rectangle(result,(512, 180),(1024, 280),(0,0,0),-1)
	textColor = (0, 255, 0) if originalClass==predictedClass else (0, 0, 255)
	result = utl.addText(result, text, (550, 350), 3, 6, textColor)
	return result
	

# Getting image files by class
testImageFiles = utl.getImageFileNamesByClass(utl.TEST_IMAGES_FOLDER_PATH)

# Reading the clusters centers.
centers = utl.readPickeAtLocation(\
	utl.CLUSTER_CENTERS_FILE_PATH
)


# Reading classifier model
print("Reading classifier model")
classifier = utl.readPickeAtLocation(\
	utl.CLASSIFIER_FILE_PATH
)


testImageFilesLabeled = []
Append = testImageFilesLabeled.append
for className in testImageFiles:
	for imageFile in testImageFiles[className]:
		Append((imageFile, className))

np.random.shuffle(testImageFilesLabeled)

# Creating Window that will show the video color segmentation
cv2.startWindowThread()
title = 'Object classification (Press p key to start)'
cv2.namedWindow(title)

# Flag used to control when to start or pause the video
isVideoPaused = True

for imageFile, className in testImageFilesLabeled:
	
	image = cv2.imread(imageFile)
	# imageFeatures = fe.getSIFTFeatures(image)
	#print len(imageFeatures)
 	image = utl.resizeToHeight(image, 200)
	imageFeatures = fe.getSIFTFeatures(image)

	

	classifierFeatures = fe.getClassifierFeatures(imageFeatures, centers)
	
	prediction = classifier.predict([classifierFeatures])[0]
	

	presentationImage = preparePresentationImage(\
		image,
		className,
		prediction
	)

	cv2.imshow(\
		title,
		presentationImage
	)

	# To control the flow of the  presentation.
	pressedKey = cv2.waitKey(0) & 0xFF if isVideoPaused else cv2.waitKey(5) & 0xFF
	if pressedKey == ord('q') or pressedKey == ord('Q'):
		break
	elif pressedKey == ord('p') or pressedKey == ord('P'):
		print "pause"
		isVideoPaused = not isVideoPaused

		
cv2.destroyAllWindows()