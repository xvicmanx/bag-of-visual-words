"""
Author: Karteek Bulusu  and Victor Trejo 
Description: This script contains a series of functions used to present the results.
"""

from matplotlib import pyplot as plt
import numpy as np


def printEvaluationMetrics(\
	classes, caseName,\
	correctlyPredicted, confusionMatrix,\
	truePositives, falsePositives, falseNegatives):
	"""
	Prints evaluation of the classifier algorithm
	Parameters:
				classes   			-> all possible predicting classes
				caseName  			-> Type of the case was tested (anomaly, misuse)
				correctlyPredicted	-> accuracy of the model
				confusionMatrix 	-> confusion matrix of the tested model
				truePositives       -> true positives of the tested model
				falsePositives      -> false positives of the tested model
				falseNegatives      -> false negatives of the tested model
	"""
	# Printing dividing line
	print '{:*^180}'.format('')
	# printing title
	print "PREDICTION EVALUATION METRICS FOR '{0}':\n".format(caseName)
	# printing overall accuracy
	print "Overall Accuracy = {0:.2f} %".format(correctlyPredicted)
	# Printing statistics by class
	for className in classes:
		__printClassStatistics(className, truePositives, falsePositives, falseNegatives)

	# printing confusion matrix
	__printConfusionMatrix(confusionMatrix, classes)
	print '\n{:*^180}\n\n\n'.format('')


def __printClassStatistics(className, TP, FP, FN):
	"""
	Prints all the statics metrics for a given class
	Parameters:
				className -> label of the class 
				TP 		  -> true positives data structure
				FP        -> false positives data structure
				FN        -> false negatives data structure
	"""
	# Calculating precision and recall metrics
	precision = 100 * (float(TP[className])/(TP[className] + FP[className])) if (TP[className] + FP[className])>0 else 0
	recall = 100 * (float(TP[className])/(TP[className] + FN[className])) if (TP[className] + FN[className])>0 else 0
	fScore = float(2*precision*recall)/(precision + recall)  if (precision + recall) > 0 else 0

	print "\n*** {0} CLASS STATISTICS".format(className)
	# Printing divisor line
	__printTableDivisorLine(1)
	# Printing precision Value
	__printTableLabel('F Score')
	__printTableValue("{0:.2f} %".format(fScore))
	# Printing precision Value
	__printTableLabel('Precision')
	__printTableValue("{0:.2f} %".format(precision))
	# Printing recall Value
	__printTableLabel('Recall')
	__printTableValue("{0:.2f} %".format(recall))
	# Printing true positives Value
	__printTableLabel('True Positives')
	__printTableValue(TP[className])
	# Printing false positives Value
	__printTableLabel('False Positives')
	__printTableValue(FP[className])
	# Printing false negative Value
	__printTableLabel('False Negatives')
	__printTableValue(FN[className])
	# Printing divisor line
	__printTableDivisorLine(1)


def __printConfusionMatrix(confusionMatrix, classes):
	"""
	Prints out on the screen the confusion matrix for all the classes.
	Parameters:
				confusionMatrix -> the confusion matrix data structure
				classes			-> collection of class labels.
	"""
	# Printing title
	print "\n*** CONFUSION MATRIX"
	
	# Printing lines
	__printTableDivisorLine(len(classes))
	print '{:^20}|'.format('Predicted / Real -> '),
	for cc in classes:
		print '{:^12}|'.format(cc),
	print ""
	# Printing lines
	__printTableDivisorLine(len(classes))

	for cc in classes:
		# Printing Values
		print '{:^20}|'.format(cc),
		for cp in classes:
			value = '{:^12}|'.format(confusionMatrix[cc][cp])
			print value,
		print ""

		# Printing lines
		__printTableDivisorLine(len(classes))


def __printTableDivisorLine(n):
	"""
	Prints a table division line.
	Parameters: n -> number of values of the table.
	"""
	# Printing lines
	print '{:-^20}|'.format(''),
	for i in range(n):
		print '{:-^12}|'.format(''),
	print ""

def __printTableLabel(label):
	"""
	Prints a table row label.
	Parameters: label -> label to print.
	"""
	print '{:^20}|'.format(label),

def __printTableValue(value):
	"""
	Prints a table row value.
	Parameters: value -> value to print.
	"""
	print '{:^12}|'.format(value)



def plotConfusionMatrix(\
    confusionMatrix,
    title,
    labels
):
	"""
	Plots the confusion matrix of test.
	Parameters:
				confusionMatrix -> confusion matrix information
				title 			-> title for presentation
				labels			-> labels of the confusion matrix.
	"""
	try:
		plt.imshow(\
		    confusionMatrix,
		    interpolation='nearest',
		    cmap= plt.cm.Reds
		)

		plt.title(title)
		plt.colorbar()
		tickMarks = np.arange(len(labels))
		plt.xticks(tickMarks, labels, rotation=45)
		plt.yticks(tickMarks, labels)
		plt.tight_layout()
		plt.ylabel('True class')
		plt.xlabel('Predicted class')
		plt.show()	

	except Exception, e: pass