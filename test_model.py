"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script is used to test the trained classifier in the test data.
"""

from sklearn.metrics import confusion_matrix
import numpy as np
import utilities as utl
import presentation_utils as pu
import features_extraction_utilities as fe
import matplotlib.pyplot as pl



# Reading classifier model
print("Reading classifier model")
classifier = utl.readPickeAtLocation(\
	utl.CLASSIFIER_FILE_PATH
)


# Reading the clusters centers.
print("Reading cluster centers")
centers = utl.readPickeAtLocation(\
	utl.CLUSTER_CENTERS_FILE_PATH
)

# Reading test images features
print("Reading test image features")
featuresByClass = utl.readFeaturesFilesByClass(\
	utl.TEST_IMAGES_FEATURES_FOLDER_PATH
)


print("Getting testing features")
testingModelData = []
# Saving the reference to improve speed
GetClassifierFeatures = fe.getClassifierFeatures
AppendToList = testingModelData.append
for className in featuresByClass:
	for sampleFeatures in featuresByClass[className]:
		classifierFeatures = GetClassifierFeatures(sampleFeatures, centers)
		AppendToList((list(classifierFeatures), className))


# Separating testing data into features values and classes
features = map(lambda x: x[0], testingModelData)
classes = map(lambda x: x[1], testingModelData)


# Confusion Matrix Data Structure
confusionMatrixDT = {pc:{rc:0 for rc in classes} for pc in classes}
# To store the true positives by class
truePositives = {c:0 for c in classes}
# To store  the false positives by class
falsePositives = {c:0 for c in classes}
# To store  the false negatives by class
falseNegatives = {c:0 for c in classes}
# To keep track of the number of correctly predicted samples overall
correctlyPredicted = 0

	# Printing the evaluation metrics out on the screen.
	

print("Making predictions")
setOfLabels = list(set(classes))
setOfLabels = sorted(setOfLabels)

predictions = classifier.predict(features)
confusionMatrix = confusion_matrix(\
	classes,
	predictions,
	labels=setOfLabels
)

for i in range(len(classes)):
	print "(Predicted='{0}',\tReal='{1}')".format(predictions[i], classes[i])
	predicted = predictions[i]
	real = classes[i]
	
	confusionMatrixDT[predicted][real]+=1

	if predictions[i] == classes[i]:
		correctlyPredicted+=1
		truePositives[predicted]+=1
	else:
		falsePositives[predicted]+=1
		falseNegatives[real]+=1



print "\n\n"
pu.printEvaluationMetrics( \
	setOfLabels, 
	"Objects Classification",
	100*(float(correctlyPredicted)/len(classes)),
	confusionMatrixDT, 
	truePositives,
	falsePositives, 
	falseNegatives
)


pl.figure()
pu.plotConfusionMatrix(\
	confusionMatrix,
	'Confusion matrix',
	setOfLabels
)