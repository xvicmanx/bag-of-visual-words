"""
Authors: Karteek Bulusu  and Victor Trejo 
Description: This script is used to demo the classification model.
"""

import numpy as np
import cv2
import utilities as utl
import features_extraction_utilities as fe


# Reading the clusters centers.
centers = utl.readPickeAtLocation(\
	utl.CLUSTER_CENTERS_FILE_PATH
)


# Reading classifier model
classifier = utl.readPickeAtLocation(\
	utl.CLASSIFIER_FILE_PATH
)



capturer = cv2.VideoCapture(0) 

while capturer.isOpened():
	frame = None
	try:
		ret, frame = capturer.read()
		cv2.imshow('Capturing Image to predict',frame) 
	except Exception, e:
		capturer.release()
	key = cv2.waitKey(1) & 0xFF
	if key == ord('c') or key == ord('C'):
		cv2.destroyAllWindows()
		image = utl.resizeToHeight(frame, 200)
		imageFeatures = fe.getSIFTFeatures(image)
		# imageFeatures = fe.getSIFTFeatures(frame)
		classifierFeatures = fe.getClassifierFeatures(imageFeatures, centers)
		prediction = classifier.predict([classifierFeatures])[0]

		rows, cols, channels = frame.shape
		resized = cv2.rectangle(frame,(0, 0),(cols, 50),(0,0,0),-1)
		labeledImage = utl.addText(\
			resized,
			"Predicted: " + prediction,
			(30, 35),
			1,
			3,
			(255, 255, 255)
		)

		cv2.imshow('Classification Result', labeledImage)
		key = cv2.waitKey(0)
		cv2.destroyAllWindows()

	if key == ord('q') or key == ord('Q') or key == 27:
		break

cv2.destroyAllWindows()
capturer.release()